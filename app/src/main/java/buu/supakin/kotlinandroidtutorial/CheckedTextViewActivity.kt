package buu.supakin.kotlinandroidtutorial

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.CheckedTextView
import android.widget.Toast

class CheckedTextViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checked_text_view)

        val checkedTextView = findViewById<CheckedTextView>(R.id.checkedTextView)
        checkedTextView.isChecked = false
        checkedTextView.setCheckMarkDrawable(R.drawable.uncheck)
        checkedTextView.setOnClickListener {
            checkedTextView.isChecked = !checkedTextView.isChecked
            checkedTextView.setCheckMarkDrawable(
                if (checkedTextView.isChecked) R.drawable.check
                else R.drawable.uncheck
            )

            val msg = getString(R.string.msg_shown) + " " + getString(if (checkedTextView.isChecked) R.string.checked else R.string.unchecked)
            Toast.makeText(CheckedTextViewActivity@this, msg, Toast.LENGTH_LONG).show()
        }
    }
}