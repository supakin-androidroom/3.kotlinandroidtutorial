package buu.supakin.kotlinandroidtutorial

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_show_name.*

class ShowNameActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_name)
        val name = intent.getStringExtra("name")?:"Unknown"
        val txtName = findViewById<TextView>(R.id.txtName)
        txtName.text = name
    }
}