package buu.supakin.kotlinandroidtutorial

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.Toast

class AutocompleteTextViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_autocomplete_text_view)
        val autoTextView = findViewById<AutoCompleteTextView>(R.id.autoCompleteTextView)
        val language =  resources.getStringArray(R.array.Languages)
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, language)
        autoTextView.setAdapter(adapter)

        val button = findViewById<Button>(R.id.btnSubmit)
        button.setOnClickListener{
            val enterText = getString(R.string.submitted_lang) + autoTextView.getText()
            Toast.makeText(this@AutocompleteTextViewActivity,
                    enterText,
                    Toast.LENGTH_LONG
            ).show()
        }
    }
}