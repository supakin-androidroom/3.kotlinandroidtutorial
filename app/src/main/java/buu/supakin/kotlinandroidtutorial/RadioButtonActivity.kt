package buu.supakin.kotlinandroidtutorial

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.RadioButton
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_radio_button.*

class RadioButtonActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_radio_button)

        val btnRadioSubmit = findViewById<Button>(R.id.btnRadioSubmit)
        btnRadioSubmit.setOnClickListener {
            val id = radioWaifuGroup.checkedRadioButtonId
            if (id != -1) {
                val radio = findViewById<RadioButton>(id)
                Toast.makeText(applicationContext, "Your Answer is : ${radio.text}",Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(applicationContext, "Waifu is not select by you",Toast.LENGTH_LONG).show()
            }
        }
    }

    fun radioButtonClick (view: View) {
        val radio = findViewById<RadioButton>(radioWaifuGroup.checkedRadioButtonId)
        Toast.makeText(applicationContext, "You will select : ${radio.text}",Toast.LENGTH_LONG).show()
    }
}